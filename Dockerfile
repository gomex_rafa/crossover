FROM maven:3-jdk-8-alpine as BUILD
WORKDIR /code
COPY ./Code /code
RUN mvn clean package -Dmaven.test.skip=true

FROM openjdk:8
COPY --from=BUILD /code/target/journals-1.0.jar /code/target/journals-1.0.jar
EXPOSE 8000
CMD ["/usr/bin/java", "-jar", "/code/target/journals-1.0.jar", "--server.port=8000"]
